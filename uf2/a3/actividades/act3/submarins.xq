let $myDocument := doc("submaris.xml")
return
<html>
  <head>
    <link rel="stylesheet" type="text/css" href="estil_SOLUCIO.css" />
  </head>
  <body>
    <table border="0" align="center">
      <tr>
        <th>
        {
          for $i in doc("submaris.xml")/submarinos/titulo
          return data($i)
        }
        </th>
       </tr>
       
       <tr>
        <td>
          <img src="banderes/{
            for $i in doc('submaris.xml')/submarinos/logo
            return $i
          }" id="esquinarImagen"></img>
        </td>
      </tr>
    </table>
    
    <table id="t01" border="1" align="center">
      <tr>
        <th class="normal">Nom</th>
        <th class="normal" width="410" align="center">
          Propulsió
        </th>
        <th class="normal" width="220" align="center">
          Torpedes
        </th>
        <th class="normal" width="250" align="center">
          Missils
        </th>
      </tr>
        {
          for $i in doc("submaris.xml")/submarinos/submarino
              let $propulsion := $i/caracteristicas_generales/propulsion
              let $powa := $i/caracteristicas_generales/potencia_watios
              let $poca := $i/caracteristicas_generales/potencia_caballos
              let $tipotor := $i/armamento/armas/torpedos/torpedo_identificador/nombre
              let $numtor := $i/armamento/armas/torpedos/torpedo_identificador/nombre/@cantidad
              let $tipomis := $i/armamento/armas/misiles/misil_identificador/nombre
              let $nummis := $i/armamento/armas/misiles/misil_identificador/nombre
          return 
          <tr>
            <td>
              <img width="256px" src="imatges/{data($i/imatges/foto)}" id="responsive-image" />
              <br></br> 
              <center><a href="{data($i/pag_web)}">{data($i/@nom)}</a></center>
            </td>
            <td align="right">
              {data($propulsion)} de {data($powa)}({data($poca)})
            </td>
            
            <td align="right">
              {data($numtor)} del tipus {data($tipotor)}<b></b>
            </td>
            
            <td align="right">
              {data($nummis)} del tipus {data($tipomis)}<b></b>
            </td>
          </tr>
        }
        <tr>
          <td colspan="2">
      {
         let $i := doc("submaris.xml")/submarinos/submarino/sum(unidades_planeadas)
              let $sumpla := $i
         return
            <b>Unitats totals plantejades: {sum(data($sumpla))}</b>
      }       
      </td>
      
      <td colspan="2" align="right">
        {
         let $i := doc("submaris.xml")/submarinos/submarino/sum(unidades_construidas)
              let $sumcon := $i
         return
            <b>Unitats totals plantejades: {sum(data($sumcon))}</b>
      }
      </td>
      
      </tr>
    </table>
  </body>
</html>