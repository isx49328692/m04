for $i in doc("cotxes.xml")/tenda/vehicles/vehicle
let $preu:=$i/preu
order by number($preu) descending
return data($preu)