for $i in doc("cotxes.xml")/tenda/vehicles/vehicle
let $preu := $i/preu
where $preu > 10000
order by number($preu) descending
return data($preu)