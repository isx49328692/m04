declare function local:maxim($valor1 as xs:decimal?, $valor2 as xs:decimal?) as xs:decimal?
{
  max(($valor1, $valor2))
};

let $i := 60
let $j := 90
return (local:maxim($j, $i))