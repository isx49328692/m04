declare function local:despeses($preu as xs:float) as xs:float 
{
  ($preu * 0.08) + 3
};

declare function local:suma($n1 as xs:float, $n2 as xs:float)as xs:float
{
  $n1 + $n2
};

for $i in doc("playstore.xml")/playstore/jocs/joc
let $despeses :=(local:despeses($i/preu))
let $total :=(local:suma($i/preu,$despeses))
return(<paquet>{$i/nom,$i/preu,<despeses>{$despeses}</despeses>,<total>{$total}</total>}</paquet>)