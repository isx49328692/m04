for $i in doc("playstore.xml")/playstore/jocs/joc
let $nom := $i/nom
let $comp := $i/companyia
where contains($i, 'ge')
return ($nom, $comp)