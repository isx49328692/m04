for $i in doc("playstore.xml")/playstore/jocs/joc
let $j := $i/nom
order by $i/preu
return if ($i/preu < 50)
then <barat>{data($j), data($i/preu)} </barat>
else <car>{data($j), data($i/preu)} </car>