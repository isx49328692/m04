let $myDocument := doc("libreria.xml")
return 
<html>
  <body>
    <table border="1" align="center">
      <tr>
        <td align="left"><b>TITOL</b></td>
        <td align="left"><b>EDITORIAL</b></td>
        <td align="right"><b>PREU</b></td>
       </tr>
      {
      for $i in (doc("libreria.xml") /bib/libro)
      return
        <tr>
         <td><b>{data($i/titulo)}</b></td>
         <td><b>{data($i/editorial)}</b></td>
         <td align="right"><b>{data($i/precio)}</b></td>
        </tr>
      }
    </table>
  </body>
</html>