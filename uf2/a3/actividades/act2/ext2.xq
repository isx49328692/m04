for $i in doc("libreria.xml") /bib/libro
let $j := $i/@anyo
order by $j
return ($j, $i/titulo)