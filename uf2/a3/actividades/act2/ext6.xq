for $i in doc("libreria.xml") /bib/libro
where count($i/autor) > 1
return ($i/@anyo, $i/titulo)