for $i in distinct-values(doc("libreria.xml") /bib/libro/autor/apellido)
order by $i
return ($i)