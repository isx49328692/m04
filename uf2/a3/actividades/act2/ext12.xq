let $myDocument := doc("libreria.xml")
return 
<html>
  <body>
    <table border="1" align="center">
      <tr>
       <td colspan="2">
         <h2>LLibres de Stevens</h2>
        </td>
      </tr>
      <tr>
       <td align="center"><b>TITOL</b></td>
       <td align="center"><b>PREU</b></td>
      </tr>
      {
      for $i in (doc("libreria.xml")/bib/libro)
      where $i/autor/apellido = "Stevens"
      return
        <tr>
         <td>{data($i/titulo)}</td>
         <td align="right">{data($i/precio)}</td>
        </tr>
      }
      {
        let $sumaPreus := sum(doc("libreria.xml")/bib/libro[autor/apellido="Stevens"]/precio)
        return
          <tr>
           <td align="right"><b>Preu total</b></td>
           <td align="right">{data($sumaPreus)}</td>
          </tr>
      }
    </table>
  </body>
</html>