for $i in doc("libreria.xml") /bib/libro
where $i/@anyo > 1992 and $i/editorial = "Addison-Wesley"
return ($i/@anyo, $i/titulo)