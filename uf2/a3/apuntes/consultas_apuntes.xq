doc("apunts1.xml")/institut/alumnes/alumne

doc("apunts1.xml")/institut/alumnes/alumne[edat>25]

for $i in doc("apunts1.xml")/institut/alumnes/alumne
return $i /nom

for $i in doc("apunts1.xml")/institut/alumnes/alumne
return ($i/nom, $i/cognoms)

for $i in doc("apunts1.xml")/institut/alumnes/alumne
let $j:=$i /edat
return $j

for $i in doc("apunts1.xml")/institut/alumnes/alumne
let $j:=$i /edat
order by $j
return $j

for $i in doc("apunts1.xml")/institut/alumnes/alumne
let $j:=$i /edat
where $j > 25
order by $j
return $j


for $i in doc("apunts1.xml")/institut/alumnes/alumne
let $j:=$i /edat
where $j > 25
order by $j
return data($j

(:Funciones:)

let $i := doc("apunts1.xml")/institut/alumnes/alumne/edat
return sum($i)


