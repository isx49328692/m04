<html>
<head></head>
  <body>
    <table border = "1" align="center">
      <tr>
        <td align="center">CLASSE</td>
        <td align="center">TRIPULACIO</td>
        {
          for $nau in doc("Babylon_5.xml")/babylon5/nau
          return
            <tr>
              <td align="center">{data($nau/classe/@classe)}</td>
              <td align="center">{$nau/capacitat_humana/tripulacio}</td>
            </tr>
        }
        <tr>
          <td align="right"><b>Total de naus conegudes</b></td>
          <td align="center"><b>
            {
              sum((for $nau in doc("Babylon_5.xml")/babylon5/nau
              return $nau/noms/@coneguts))
            }
            </b>
          </td>
        </tr>
      </tr>
    </table>
  </body>
</html>