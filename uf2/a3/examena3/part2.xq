<html>
  <head>
  </head>
  
  <body>
    <table border="1" align="center">
      <tr>
        <td colspan="3" align="center">
          <a href="{for $i in doc('part2.xml')/armament
        return $i/web}">
          <img width="50%" height="50%" src="imatges_y_banderes/banderes/{for $i in doc('part2.xml')/armament
          return $i/bandera}" />
        </a>
        </td>
      </tr>
      
      <tr>
        <td colspan="3" align="center">
          <b>Missils aire-aire amb sistema de guia radar</b>
        </td>
      </tr>
      
      <tr>
        <th width="256"><b>Nom</b></th>
        <th><b>Caracteŕistiques</b></th>
        <th><b>Avions portadors</b></th>
      </tr>
      
      {
        for $i in doc("part2.xml")/armament/misils_aire_aire/misil_aire_aire
        order by $i/anys_produccio
        return
        <tr>
            <td>
            <a href="{$i/pag_web}">
              <img src="imatges_y_banderes/imatgesAvions/{
                $i/foto
              }" width="256"/>
            </a>
              <br></br>
              <center><a href="{data($i/pag_web)}">{data($i/nom_otan)}</a></center>
            </td>
            
            <td>
              Pes: {
                $i/pes
              }
              <br></br>
              Velocitat: {
                $i/velocitat_mach
              }
              <br></br>
              Sistema de guia: <b>{
                $i/sistema_de_guia
              }</b>
              <br></br>
              Anys de producció: {
                $i/anys_produccio
              }
            </td>
            
            <td align="right">
              {$i/avions/avio}
            </td>
        </tr>
      }
      
      <tr>
        <td colspan="3" align="center">
          {
            let $i := doc("part2.xml")/armament/misils_aire_aire/misil_aire_aire[sistema_de_guia='radar']
            let $cra := count($i)
            return <center><b>Unitats amb sistema de guis radar: </b>{data($cra)}</center>
          }
          <br></br>
          {
            let $i := doc("part2.xml")/armament/misils_aire_aire/misil_aire_aire[sistema_de_guia!='radar']
          let $cdif := count($i)
          return <center><b>Unitats amb altres sistemes de guia: </b>{$cdif}</center>
          }
        </td>
      </tr>
      
    </table>
  </body>
</html>