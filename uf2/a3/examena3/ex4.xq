for $nau in doc("Babylon_5.xml")/babylon5/nau
order by $nau/origen
return
  if (contains($nau/origen, 'Earth_Alliance'))
  then <Earth_Alliance> {data($nau/classe/@classe)} amb potencia militar de {data($nau/propulsio/potencia_militar), data($nau/propulsio/unitat_de_mesura_potencia)} </Earth_Alliance>
  else 
  if (contains($nau/origen, 'Narn'))
  then <Narn> {data($nau/classe/@classe)} amb potencia militar de {data($nau/propulsio/potencia_militar), data($nau/propulsio/unitat_de_mesura_potencia)} </Narn>
  else <nda></nda>