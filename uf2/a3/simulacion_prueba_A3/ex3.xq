for $peli in doc("cine.xml")/collection/movie
order by $peli/type
return
	if (contains($peli/type, 'War'))
	then <guerra> {(data($peli/@title), data($peli/type))} </guerra>
  else
	if (contains($peli/type, 'Action'))
	then <accio> {(data($peli/@title), data($peli/type))} </accio>
  else
	if (contains($peli/type, 'Science Fiction'))
	then <cifi> {(data($peli/@title), data($peli/type))} </cifi>
	else <sin></sin>