<html>
  <head>
  </head>
	<body>
		<table border="1" align="center">
			<tr>
				<td>TITOL</td>
				<td>ANY</td>
			</tr>
			{
			for $i in doc("cine.xml")/collection/movie
			where contains($i/type, 'Science Fitcion')
			order by number($i/year)
			return
				<tr>
					<td> {data($i/@title)} </td>
					<td> {data($i/year)} </td>
				</tr>
			}
      {
        for $i in doc("cine.xml")/collection/movie
        where contains($i/type, 'Science Fiction')
        order by number($i/year)
        return
        <tr>
        <td>{data($i/@title)}</td>
        <td>{data($i/year)}</td>
        </tr>
      }
			
			{
			<tr>
				<td align="right">Total de pelis</td>
				<td align="center">
					{
					count(for $i in doc("cine.xml")/collection/movie
					where contains($i/type, 'Science Fiction')
					return $i)
					}
				</td>
			</tr>
			}
		</table>
	</body>
</html>