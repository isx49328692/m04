let $myDocument := doc("Babylon_5.xml")
return
<html>
  <head>
    <link rel="stylesheet" type="text/css" href="estil_SOLUCIO.css" />
  </head>
  
  <body>
    <table border="0" align="center">
      <tr>
        <td>
          <a href="{for $i in doc('Babylon_5.xml')/babylon5/dades_generals/web
          return $i}">
            <img src="imagenes_y_logos/{
              for $i in doc('Babylon_5.xml')/babylon5/dades_generals
            return $i/logo}" id="esquinarImagen"/>
          </a>
        </td>
      </tr>
    </table>
    
    <table width="900" id="t01" border="1" align="center">
      <tr>
        <th colspan="4">
          {
            let $i := doc("Babylon_5.xml")/babylon5/nau/noms/nom
            let $nomnav := count($i)
            return
              <center>Nº de noms de naus = {data($nomnav)}</center>
          }
          {
            let $i := doc("Babylon_5.xml")/babylon5/nau/noms/@coneguts
            let $sumcone := $i
            return
              <center>Nº de noms de naus coneguts = {sum(data($sumcone))}</center>
          }
          {
            let $i := doc("Babylon_5.xml")/babylon5/nau[origen='Earth_Alliance']
            let $sumcone := $i/noms/@coneguts
            return
              <center>Nº de noms de naus coneguts de origen Earth_Alliance = {sum(data($sumcone))}</center>
          }
          {
            let $i := doc("Babylon_5.xml")/babylon5/nau[origen='Narn']
            let $sumcone := $i/noms/@coneguts
            return
              <center>Nº de noms de naus coneguts de origen Narn = {sum(data($sumcone))}</center>
          }
        </th>
      </tr>
      
      <tr>
        <th class="normal" width="320">Nom</th>
        <th class="normal" width="120">Bandol</th>
        <th class="normal" width="320">Característques</th>
        <th class="normal" width="140">Noms coneguts</th>
      </tr>
      {
        for $i in doc("Babylon_5.xml")/babylon5/nau
        return
          <tr>
            <td>
              <img src="imagenes_y_logos/{
                $i/imatge
              }" width="100%" height="auto" />
              <br></br>
              <center><a href="{data($i/pag_web)}">{data($i/classe/@classe)}</a></center>
            </td>
            
            <td>
              <img src="imagenes_y_logos/{
                $i/logo
              }" />
            </td>
            
            <td>
              Tonelatge: {
                concat($i/caracteristiques/tonelatge,
               "de", $i/caracteristiques/tonelatge/@unitat)}
               <br></br>
               Longitud: {
                 $i/caracteristiques/longitud
               }
               <br></br>
               Tripulacio:{
                 $i/capacitat_humana/tripulacio
               }
               Naus transportades:<br></br>
               &#160;&#160;&#160;&#160;&#160;Caces: {
                 $i/capacitat_naus/caces
               }
               <br></br>
               &#160;&#160;&#160;&#160;&#160;Transport de tropes: {
                 $i/capacitat_naus/transports_tropes
               }
               <br></br>
               Abast: {
                 $i/abast/quantitat
               }&#160;
               {
                 $i/abast/unitat_de_mesura
               }
               <br></br>
               Propulsió:<br></br>
               &#160;&#160;&#160;&#160;&#160;Reactors: {
                 data($i/propulsio/reactors/@numero)
               }&#160;
               {
                 $i/propulsio/reactors
               }
               <br></br>
               &#160;&#160;&#160;&#160;&#160;Motors de impulsio: {
                 $i/propulsio/numero_de_motors_impulsio
               }&#160;
               {
                 $i/propulsio/motors_impulsio
               }
               <br></br>
               &#160;&#160;&#160;&#160;&#160;Potencia standart: {
                 $i/propulsio/potencia_standart
               }&#160;
               {
                 $i/propulsio/unitat_de_mesura_potencia
               }
               <br></br>
               &#160;&#160;&#160;&#160;&#160;Potencia millitar: {
                 $i/propulsio/potencia_militar
               }&#160;
               {
                 $i/propulsio/unitat_de_mesura_potencia
               }
            </td>
            
            <td>
              {
                $i/noms/nom
              }
              <br></br>
              (Nº noms = {
                count($i/noms/nom)
              })
            </td>
          </tr>
      }
    </table>
  </body>
</html>