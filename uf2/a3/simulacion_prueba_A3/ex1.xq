for $peli in doc("cine.xml")/collection/movie[1979 < year and 1990 > year]
where contains($peli/type, 'Science Fiction')
order by number($peli/year) descending
return <resultat> {(data($peli/year), data($peli/@title))} </resultat>