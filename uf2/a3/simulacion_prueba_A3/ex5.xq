for $i in doc("cine.xml")/collection/movie
let $j := $i/stars
let $k := $i/type
group by $k
return
	<resultat>Tipus: {data($k)} nº estrelles: {sum($j)}</resultat>