for $peli in doc("cine.xml")/collection/movie
where $peli/year > 1980 and 1989 > $peli/year
order by number($peli/year) descending
return
	if (contains($peli/@title, 'Star Trek'))
	then <b>{(data($peli/year), data($peli/@title))}</b>
	else (data($peli/year), data($peli/@title))