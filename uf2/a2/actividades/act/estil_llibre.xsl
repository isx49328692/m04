<?xml version="1.0" encoding="UTF-8"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"> 
	<xsl:template match="/"> 
		<html>
			<body>
				<table border="1" align="center">
					<xsl:for-each select="bookstore/book[year=2003 and price >= 30]">
						<xsl:sort select="@category" order="descending"/>
						<tr>
							<td colspan="2" bgcolor="grey"><span style="color: yellow;">Categoria:</span> <xsl:value-of select="@category"/></td>
						</tr>
						<tr>
							Titol:<td><xsl:value-of select="title"/></td>
						</tr>
						<tr>
							Any:<td><xsl:value-of select="year"/></td>
						</tr>
						<tr>
							Preu:<td><xsl:value-of select="price"/></td>
						</tr>
						<tr>
							<th bgcolor="cyan" colspan="2">Autors</th>
						</tr>
						<tr>
							<xsl:for-each select="author">
								<xsl:sort select="."/>
								<tr>
									<td><xsl:value-of select="."/></td>
								</tr>
							</xsl:for-each>
						</tr>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>