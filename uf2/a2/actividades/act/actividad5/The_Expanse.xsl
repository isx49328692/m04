<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template match="/">
<html>
	<head>
		<title><xsl:value-of select="the_expanse/dades/titol"/></title>
		<link href="imatges_i_logos/css/estil_SOLUCIO.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<table border="0" align="center">
			<tr>
				<td width="100%" height="auto">
				<a>
					<xsl:attribute name="href">
						<xsl:value-of select="the_expanse/dades/pag_web"/>
					</xsl:attribute>
					<img id="esquinarImagen" align="center">
						<xsl:attribute name="src">
						imatges_i_logos/logos/<xsl:value-of select="the_expanse/dades/logo"/>
						</xsl:attribute>
					</img>
				</a>
				</td>
			</tr>
		</table>
		
		<table border="1" id="t01" width="1050" align="center">
			<tr>
				<th>Nom</th>
				<th>Bandol</th>
				<th>Caracteristiques</th>
				<th>Noms coneguts</th>
			</tr>
			
			<xsl:for-each select="the_expanse/Martian_Congressional_Republic_Navy/nau">
				<xsl:sort select="tipus"/>
				<xsl:if test="caracteristiques/longitud &gt; 10">
					<tr>
						<td width="320">
							<a>
								<xsl:attribute name="href">
									<xsl:value-of select="web"/>
								</xsl:attribute>
								<img id="responsive-image">
									<xsl:attribute name="src">
										imatges_i_logos/imatges/<xsl:value-of select="imatge"/>
									</xsl:attribute>
								</img>
							</a><br></br>
							<center><b>Classe <xsl:value-of select="classe"/></b></center>
						</td>
						
						<td width="120">
							<img id="responsive-image">
								<xsl:attribute name="src">
								imatges_i_logos/logos/<xsl:value-of select="logo"/>
								</xsl:attribute>
							</img>
						</td>
						
						<td width="400" class="normal">
							Tipus de nau:<xsl:value-of select="tipus"/><br></br>
							Tonelatge:<xsl:value-of select="caracteristiques/tonelatge"/>de<xsl:value-of select="caracteristiques/tonelatge/@unitat"/><br></br>
							Longitud:<xsl:value-of select="caracteristiques/longitud"/><br></br>
							Tripulacio:<xsl:value-of select="capacitat_humana/tripulacio"/><br></br>
							<xsl:if test="capacitat_humana/tropes/@enPorta='Si'">
							<b>Tropes:<xsl:value-of select="capacitat_humana/tropes"/></b><br></br>
							</xsl:if>
							Propulsio:<xsl:value-of select="propulsio/numero_de_motors"/><xsl:value-of select="propulsio/motors_impulsio"/><br></br>
						</td>
						
						<td width="240" class="normal">
							<xsl:for-each select="designacions/nom">
								<xsl:value-of select="."/>
							</xsl:for-each>
							<br/>
							(Nº noms = <xsl:value-of select="count(designacions/nom)"/>
						</td>
					</tr>
				</xsl:if>
			</xsl:for-each>
		</table>
	</body>
</html>
</xsl:template>
</xsl:stylesheet>
