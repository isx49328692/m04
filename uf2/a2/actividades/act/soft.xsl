<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
    <html>
        <head>
            <title>Activitat 1 XSLT</title>
        </head>
        <body>
            <table border="1" align="center">
                <tr align="center">
                    <td colspan="3"><b>Clasificacio</b></td>
                </tr>
                <tr bgcolor="green" align="center">
                    <td>Plataforma del programa</td>
                    <td>Nom</td>
                    <td>Tipus de llicencia</td>
                </tr>
                <xsl:for-each select="programes/editors_XML/programa">
                    <tr>
                        <td align="left"><xsl:value-of select="plataforma"/></td>
                        <td bgcolor="red" align="center"><b><xsl:value-of select="nom"/></b></td>
                        <td align="left"><xsl:value-of select="llicencia"/></td>
                    </tr>
                </xsl:for-each>
            </table>
        </body>
    </html>
</xsl:template>
</xsl:stylesheet>