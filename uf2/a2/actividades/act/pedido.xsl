<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body>
                <table border="1" align="center">
                    <tr bgcolor="grey">
                        <th colspan="4" align="center"><b>Comanda</b></th>
                    </tr>

                    <tr>
                        <th>Nom</th>
                        <th>Adreça</th>
                        <th>Ciutat</th>
                        <th>C.P.</th>
                    </tr>

                    <tr>
                        <td bgcolor="#0174DF"><b><xsl:value-of select="/Pedido/Destino/Nombre"/></b></td>
                        <td><xsl:value-of select="/Pedido/Destino/Direccion"/></td>
                        <td><xsl:value-of select="/Pedido/Destino/Ciudad"/></td>
                        <td><xsl:value-of select="/Pedido/Destino/CodPostal"/></td>
                    </tr>

                    <tr>
                        <th colspan="4">&#160;</th>
                    </tr>

                    <tr>
                        <td colspan="4" align="center">Llistat amb "Precio &#62; 25" i "Precio &#60;= 100"</td>
                    </tr>

                    <tr>
                        <th colspan="2">Producte</th>
                        <th>Preu</th>
                        <th>Quantitat</th>
                    </tr>

                    <xsl:for-each select="Pedido/Contenido/Producto">
                        <xsl:sort select="Nombre"/>
                            <xsl:if test="Precio &#62; 25 and Precio &#60;&#61; 100">
                                <tr>
                                    <xsl:choose>
                                        <xsl:when test="Precio &#62; 25 and Precio &#60; 50">
                                            <td colspan="2"><xsl:value-of select="Nombre"/></td>
                                            <td><xsl:value-of select="Precio"/></td>
                                            <td colspan="3" bgcolor="yellow"><xsl:value-of select="Cantidad"/></td>
                                        </xsl:when>

                                        <xsl:when test="Precio &#62; 50 and Precio &#60; 75">
                                            <td colspan="2"><xsl:value-of select="Nombre"/></td>
                                            <td><xsl:value-of select="Precio"/></td>
                                            <td colspan="3" bgcolor="green"><xsl:value-of select="Cantidad"/></td>
                                        </xsl:when>

                                        <xsl:otherwise>
                                            <td colspan="2"><xsl:value-of select="Nombre"/></td>
                                            <td><xsl:value-of select="Precio"/></td>
                                            <td colspan="3" bgcolor="red"><xsl:value-of select="Cantidad"/></td>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </tr>
                            </xsl:if>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>