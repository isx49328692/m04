<?xml version="1.0" encoding="UTF-8"?> 
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"> 
	<xsl:template match="/"> 
		<html> 
			<head> 
				<title>M04-UF2-NF2-Pe1</title> 
				<link href="estil_solucio.css" rel="stylesheet" type="text/css" /> 
			</head>
			<body>
				<h1 align="center">Agencia Estatal de Metereologia - AEMET.Gobierno de España</h1>
				<a href="{root/origen/web}"><xsl:value-of select="root/origen/web" /></a>
                <p>Dades per a: <span><xsl:value-of select="root/nombre"/> - <xsl:value-of select="root/provincia" /></span></p>

				<table border="1" align="center">
					<xsl:for-each select="root/prediccion/dia">
						<xsl:sort select="@fecha"/>
						<tr>
							<td>Data: <span><xsl:value-of select="@fecha"/></span></td>
						</tr>
						<tr>
							<td colspan="3" align="center" class="trey">Probabilitat precipitacio</td>
						</tr>

						<xsl:for-each select="prob_precipitacion">
							<tr>
								<td><xsl:value-of select="@periodo"/></td>
								<td colspan="2"><xsl:value-of select="."/></td>
							</tr>
						</xsl:for-each>

						<tr>
							<td colspan="3" align="center" class="trey">Dades del vent</td>
						</tr>

						<tr>
							<th class="bggrey">Periodo</th>
							<th class="bggrey">Direccion</th>
							<th class="bggrey">Velocidad</th>
						</tr>

						<xsl:for-each select="viento">
							<tr>
								<xsl:choose>
									<xsl:when test="velocidad &#62; 8">
										<td><xsl:value-of select="@periodo"/></td>
										<td><xsl:value-of select="direccion"/></td>
										<td class="venr"><xsl:value-of select="velocidad"/></td>
									</xsl:when>
									<xsl:otherwise>
										<td><xsl:value-of select="@periodo"/></td>
										<td><xsl:value-of select="direccion"/></td>
										<td class="venv"><xsl:value-of select="velocidad"/></td>
									</xsl:otherwise>
								</xsl:choose>
							</tr>
						</xsl:for-each>
						<tr>
							<td colspan="3" align="center" class="bggrey"><b>Temperatura/UV</b></td>
						</tr>
						<tr>
							<th class="bggrey">Temp. Maxima</th>
							<th class="bggrey">Temp. Minima</th>
							<th class="bggrey">UV</th>
						</tr>
						<xsl:for-each select="temperatura">
							<tr>
								<td><xsl:value-of select="maxima"/></td>
								<td><xsl:value-of select="minima"/></td>
								<td><xsl:value-of select="../uv_max"/></td>
							</tr>
						</xsl:for-each>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
