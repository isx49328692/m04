<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <html>
            <head>
                <title>
                    <xsl:value-of select="Star_Trek/dades/titol" />
                    <link href="css/estil_SOLUCIO.css" rel="stylesheet" type="text/css" />
                </title>
            </head>

            <body>
                <table border="0" align="center">
                    <tr>
                        <td width="100%" height="auto">
                            <a>
                                <xsl:attribute name="href">
                                    <xsl:value-of select="Star_Trek/dades/web"/>
                                </xsl:attribute>
                                <img id="esquinarImagen" align="center">
                                    <xsl:attribute name="src">
                                        imatges_i_CSS_PELS_ALUMNES/imatges/<xsl:value-of select="Star_Trek/dades/logo"/>
                                    </xsl:attribute>
                                </img>
                            </a>
                        </td>
                    </tr>
                </table>

                <table border="1" id="t01" width="1120" align="center">
                    <tr>
                        <th colspan="4">
                            Nº de naus en el XML = <xsl:value-of select="count(//nau)"/><br></br>
                            Nº de quantitat construides consegudes = <xsl:value-of select="sum(//@conegudes)"/><br></br>
                            Nº de naus en el XML d'origen ST:TNG = <xsl:value-of select="count(Star_Trek/naus/nau[origen='ST:TNG'])"/><br></br>
                            Nº de naus construides d'origen ST:TOS = <xsl:value-of select="sum(Star_Trek/naus/nau[origen='ST:TOS']/quantitat_construides)"/><br></br>
                        </th>
                    </tr>

                    <tr>
                        <th>Classe</th>
                        <th>Característiques</th>
                        <th>Atac i defensa</th>
                        <th>Quantitat</th>
                    </tr>

                    <xsl:for-each select="Star_Trek/naus/nau">
                        <xsl:sort select="tripulacio/tripulacio_standart" order="ascending"/>
                        <xsl:if test="tonelatge &gt; 1">
                            <tr>
                                <td width="320">
                                    <a>
                                        <xsl:attribute name="href"> 
                                            <xsl:value-of select="dades_digitals/pag_web" /> 
                                        </xsl:attribute> 
                                        <img id="responsive-image">
                                            <xsl:attribute name="src"> 
                                                imatges_i_CSS_PELS_ALUMNES/imatges/<xsl:value-of select="dades_digitals/imatge"/>
                                            </xsl:attribute> 
                                        </img>
                                    </a> 
                                    <br></br>
                                    <center><b><xsl:value-of select="classe"/></b></center>
                                </td>
            
                                <td width="120" class="normal">
                                    Longitud = <xsl:value-of select="dimensions/longitud"/> <xsl:value-of select="dimensions/@unitat"/><br/>
                                    Amplada = <xsl:value-of select="dimensions/amplada"/> <xsl:value-of select="dimensions/@unitat"/><br/>
                                    Altura = <xsl:value-of select="dimensions/altura"/> <xsl:value-of select="dimensions/@unitat"/><br/>
                                    Cobertes = <xsl:value-of select="dimensions/cobertes"/> <xsl:value-of select="dimensions/@unitat"/><br/>
                                    Tonelatge = <xsl:value-of select="tonelatge"/> <xsl:value-of select="tonelatge/@unitat"/><br/>
                                    <b>Tripulacio standart: <xsl:value-of select="tripulacio/tripulacio_standart"/></b><br/>
                                    Velocitat normal de creuer = <xsl:value-of select="velocitat_warp/velocitat_normal_de_creuer"/><br/>
                                    Velocitat maxima de creuer = <xsl:value-of select="velocitat_warp/velocitat_maxima_de_creuer"/><br/>
                                    Velocitat maxima = <xsl:value-of select="velocitat_warp/velocitat_maxima"/><br/>
                                </td>
                    
                                <td width="540" class="normal">
                                    Armament:<br/>
                                    &#160;&#160;&#160;&#160;&#160;Fasers<xsl:value-of select="atac_i_defensa/armament/fasers_quantitat"/> <xsl:value-of select="atac_i_defensa/armament/fasers_tipus"/> de <xsl:value-of select="atac_i_defensa/armament/fasers_potencia"/><br/>
                                    &#160;&#160;&#160;&#160;&#160;Torpedes<xsl:value-of select="atac_i_defensa/armament/torpedes_quantitat_tubs"/> <xsl:value-of select="atac_i_defensa/armament/torpedes_tipus"/><br/>
                                    &#160;&#160;&#160;&#160;&#160;Torpedes emmagatzemats:<xsl:value-of select="atac_i_defensa/armament/torpedes_emmagatzemats"/><br/>
                                    Defensa:<br/>
                                    &#160;&#160;&#160;&#160;&#160;Potencia escuts: <xsl:value-of select="atac_i_defensa/sistemes_defensius/potencia_escuts"/> <xsl:value-of select="atac_i_defensa/sistemes_defensius/potencia_escuts/@unitat"/><br/>
                                    <xsl:if test="atac_i_defensa/sistemes_defensius[doble_casc='si']">
                                        &#160;&#160;&#160;&#160;&#160;<b>Casc: <xsl:value-of select="atac_i_defensa/sistemes_defensius/material_del_casc"/>(doble)</b><br/>
                                    </xsl:if>
                                    <xsl:if test="atac_i_defensa/sistemes_defensius[doble_casc='no']">
                                        &#160;&#160;&#160;&#160;&#160;Casc: <xsl:value-of select="atac_i_defensa/sistemes_defensius/material_del_casc"/>(simple)<br/>
                                    </xsl:if>
                                </td>
            
                                <td width="140" class="normal">
                                    Construides: <xsl:value-of select="quantitat_construides"/>
                                    Destruides: <xsl:value-of select="quantitat_construides/@destruides"/>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="4" class="centrado">
                                    <a href="{dades_digitals/pag_web}"><xsl:value-of select="dades_digitals/pag_web" /></a>
                                </td>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>